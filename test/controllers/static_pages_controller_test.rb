require 'test_helper'

class StaticPagesControllerTest < ActionDispatch::IntegrationTest
  test 'Should get home' do
    get root_path
    assert_response :success
  end
end
