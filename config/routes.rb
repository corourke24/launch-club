Rails.application.routes.draw do
 
  get 'users/new'

  root 'static_pages#home'
  get '/services',        to: 'static_pages#services'
  get '/about',           to: 'static_pages#about'
  get '/contact',         to: 'messages#new', as: 'new_message'
  post '/contact',        to: 'messages#create', as: 'create_message'
  get '/signup',          to: 'users#new'
  
  get '/test', to: 'static_pages#test'
end
